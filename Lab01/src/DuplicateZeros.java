public class DuplicateZeros {
    static int[] numbers = {1,0,2,3,0,4,5,0};

    static void printNumbers(){
        for(int i = 0;i < numbers.length;i++){
            System.out.print(numbers[i]+" ");
        }
        System.out.println();
    }
    static void duplicateZeros() {
        for(int i = 0; i<numbers.length; i++) {
            if(numbers[i] ==0) {
                int j;
                for(j = numbers.length-2; j>=i+1; j--) {
                    numbers[j+1] = numbers[j];
                }
                numbers[j+1] = 0;
                i++;
            }
        }
    }
    public static void main(String[] args) {
        printNumbers();
        duplicateZeros();
        printNumbers();
        
    }
    
}

