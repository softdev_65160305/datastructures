import java.util.Arrays;

public class MergeSortedArrays {
    static int[] nums1 = {1,2,3,0,0,0};
    static int[] nums2 = {2,5,6};
    static int m = 3;
    static int n = 3;

    static void merge(){
        for(int i = 0;i < n ;i++){
            nums1[i+m] = nums2[i];
        }
    }
    static void printNums(){
        System.out.println("The result of the merge is");
        for(int i = 0;i < nums1.length;i++){
            System.out.print(nums1[i]+" ");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        merge();
        Arrays.sort(nums1);
        printNums();
    }    
}
