import java.util.Arrays;

public class ArrayManipulation{
    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];

       
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i]+" ");
        }
        System.out.println();
       
        for (int i =0;i<names.length;i++) {
            System.out.print(names+" ");
        }
        System.out.println();
    
        for (int i =0;i<values.length;i++) {
            values[i] = i;
            System.out.print(values[i]+" ");
        }
        System.out.println();
        
        int sum = 0;
        for (int i =0;i<numbers.length;i++) {
            sum += numbers[i];
        }
        System.out.print(sum+" ");

        System.out.println();
        
        double max = values[0]; 

        for (int i = 1; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }
        System.out.print(max+" ");

        System.out.println();
        
        String[] reversedNames = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            reversedNames[i] = names[names.length - 1 - i];
            System.out.print(reversedNames[i]+" ");
        }
        System.out.println();
        
        Arrays.sort( numbers );
        System.out.print(Arrays.toString( numbers ));
    }
}
