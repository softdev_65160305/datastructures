import java.util.Arrays;

public class ArrayDeletionsLab {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int index = 2;
        int value = 4;

        System.out.println("Original Array: " + Arrays.toString(arr));

        int[] updatedArray = deleteElementByIndex(arr, index);
        System.out.println("Array after deleting element at index " + index + ": " + Arrays.toString(updatedArray));

        updatedArray = deleteElementByValue(arr, value);
        System.out.println("Array after deleting element with value " + value + ": " + Arrays.toString(updatedArray));
    }

    public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            throw new IndexOutOfBoundsException("Invalid index");
        }

        int[] newArr = new int[arr.length - 1];
        int j = 0;

        for (int i = 0; i < arr.length; i++) {
            if (i != index) {
                newArr[j] = arr[i];
                j++;
            }
        }

        return newArr;
    }

    public static int[] deleteElementByValue(int[] arr, int value) {
        int index = -1;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            return arr; // Value not found, return the original array
        }

        return deleteElementByIndex(arr, index);
    }
}

