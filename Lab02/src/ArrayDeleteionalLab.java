import java.util.Scanner;

public class ArrayDeleteionalLab{
    static Scanner kb = new Scanner(System.in);
    static int[] arr = {1,2,3,4,5};
    
    static int[] deleteElementByIndex(int[] arr,int index){
        int[] newArray = new int[arr.length-1];
        int j = 0;
        for(int i = 0;i < arr.length;i++){
            if(i != index){
                newArray[j] = arr[i];
                j++;
            }
        }

        for(int i = 0;i < newArray.length;i++){
            System.out.print(newArray[i]+" ");
        }
        System.out.println();
        return newArray;
    }
    public static int[] deleteElementByValue(int[] arr, int value) {
        int[] newArray1 = new int[arr.length-1];
        int index = -1;

        for (int i = 0; i < arr.length; i++) {
            if (newArray1[i] == value) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            return newArray1; // Value not found, return the original array
        }

        return deleteElementByIndex(arr, index);
    }
    public static void main(String[] args) {
        int index = kb.nextInt();
        int value = kb.nextInt();
        deleteElementByIndex(arr,index);
        deleteElementByValue(arr,value);
    }
}
