public class BracketChecker {
    private String input;

    public BracketChecker(String in){
        input = in;
    }

    public void check(){
        int stackSize = input.length();
        StackXchar theStack = new StackXchar(stackSize);

        for(int j = 0;j<input.length(); j++){
            char ch = input.charAt(j);
            switch(ch)
            {
                case '{': // opening symbols
                case '[':
                case '(':
                    theStack.push(ch); // push them
                    break;

                case '}': // closing symbols
                case ']':
                case ')':
                if( !theStack.isEmpty() ){
                    char chx = theStack.pop(); // pop and check
                    if( (ch=='}' && chx!='{') ||
                        (ch==']' && chx!='[') ||
                        (ch==')' && chx!='(') )
                        System.out.println("Error: "+ch+" at "+j);
                }else
                    System.out.println("Error: "+ch+" at "+j);
                break;
            default:
                break;
            }
        }
        if ( !theStack.isEmpty())
            System.out.println("Error: missing right delimiter");
    }
}